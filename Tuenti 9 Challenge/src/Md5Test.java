import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;


public class Md5Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			long lInicio=System.currentTimeMillis();
			FileInputStream objFileIn=new FileInputStream(args[0]);
			MessageDigest objDigest=MessageDigest.getInstance("MD5");
			objDigest.reset();
			byte[] buf=new byte[4096];
			int iReaded=objFileIn.read(buf);
			while(iReaded!=-1){
				objDigest.update(buf, 0, iReaded);
				iReaded=objFileIn.read(buf);
			}
			byte[] objByteDigest= objDigest.digest();
			StringBuilder sb=new StringBuilder();
			for (byte b : objByteDigest) {
				sb.append(Integer.toHexString(0xFF & b));
			}
			String sMD5=sb.toString();
			long lFin=System.currentTimeMillis();
			System.out.println(lFin-lInicio);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
